import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as admin from 'firebase-admin';
import { ServiceAccount } from "firebase-admin"
import * as gcpConfig from './db_setup/bookit-db-459ca338076a.json'

import { HttpExceptionFilter } from './shared/filters/http-exception.filter';

async function bootstrap() {

  const configGCP: ServiceAccount = JSON.parse(JSON.stringify(gcpConfig));

  admin.initializeApp({
    credential: admin.credential.cert(configGCP)
  });

  const app = await NestFactory.create(AppModule, {cors: {origin: 'http://localhost:888'}});

  const hostDomain = AppModule.isDev ? `${AppModule.host}:${AppModule.port}`: AppModule.host;

  const swaggerOptions = new DocumentBuilder()
    .setTitle('BookitApi')
    .setDescription('API documentation')
    .setVersion('1.0.0')
    .build();

  const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions);

  app.use('/api/docs/swagger.json', (req, res) => {
    res.send(swaggerDoc);
  });

  SwaggerModule.setup('/api/docs/', app, null, {
    swaggerUrl: `${hostDomain}/api/docs/swagger.json`,
    explorer: true,
    swaggerOptions: {
      docExpansion: "list",
      filter: true,
      showRequestDurationn: true 
    }
  })

  app.setGlobalPrefix('api');
  //app.useGlobalFilters(new HttpExceptionFilter());

  await app.listen(AppModule.port);
}
bootstrap();
