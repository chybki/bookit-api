import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { EventsModule } from './events/events.module';
import { ConfigurationService } from './shared/configuration/configuration.service';
import { Configuration } from './shared/configuration/configuration.enum';

@Module({
  imports: [EventsModule],
  controllers: [AppController],
  providers: [AppService, ConfigurationService],
})
export class AppModule {
  static host: string;
  static port: string | number;
  static isDev: boolean;

  constructor(private configurationService: ConfigurationService) {
    AppModule.port = AppModule.normalizePort(configurationService.get(Configuration.PORT));
    AppModule.host = configurationService.get(Configuration.HOST);      
    AppModule.isDev = configurationService.isDevelopment;
  }

  private static normalizePort(param: number | string): number | string{
    const portNumber: number = typeof param === 'string' ? parseInt(param, 10) : param;
    if(isNaN(portNumber)) return param;
    else if(portNumber >= 0) return portNumber;
  }
}
