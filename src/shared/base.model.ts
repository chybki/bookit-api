import { ApiPropertyOptional } from "@nestjs/swagger";

export class BaseModel {
    @ApiPropertyOptional({type: String, format: 'date-time'}) createdAt?: Date;
    @ApiPropertyOptional({type: String, format: 'date-time'}) updateAt?: Date;
    @ApiPropertyOptional() id?:string;
}