export enum Configuration {
    HOST = 'HOST',
    PORT = 'PORT',
    JWT_KEY = 'JWT_KEY',
}