export function timestampToDate(stamp: number) {
    const tmpDate = new Date(stamp * 1000);
    return new Date(Date.UTC(tmpDate.getFullYear(), tmpDate.getMonth(), tmpDate.getDate(), tmpDate.getHours())).toJSON();
}