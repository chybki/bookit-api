import { ApiProperty } from "@nestjs/swagger";
import { EventStaus } from "./enent-status.enum";

export class EventModel {
    @ApiProperty({ type: String, format: 'date-time' }) timeStart: string;
    @ApiProperty({ type: String, format: 'date-time' }) timeEnd: string;    
    @ApiProperty({ enum: EventStaus }) status: EventStaus ;
    @ApiProperty() assignedUser: string;
}