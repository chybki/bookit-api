import { Controller, Get, Body, HttpStatus, Query } from '@nestjs/common';
import { ApiTags, ApiProperty, ApiResponse, ApiOperation } from '@nestjs/swagger';
import { EventModel } from './models/event.model';
import { ApiException } from 'src/shared/api-exception.model';
import { GetOperiationId } from 'src/shared/utilities/get-operation-id';
import { EventsService } from './events.service';

@Controller('events')
@ApiTags('Events')
export class EventsController {

    constructor(private eventsServcie: EventsService) {

    }

    @Get()
    @ApiResponse({status: HttpStatus.OK, type: EventModel})
    @ApiResponse({status: HttpStatus.BAD_REQUEST, type: ApiException})
    @ApiOperation(GetOperiationId('Events', 'Get all'))
    getEvents(@Query('calendar') calendar: string, @Query('month') monthKey: string): Promise<Array<EventModel>> {        
       return this.eventsServcie.fetch(calendar, monthKey).then(res => res);
    }
}
