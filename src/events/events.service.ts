import { Injectable } from '@nestjs/common';
import * as admin from 'firebase-admin';
import { EventModel } from './models/event.model';
import { timestampToDate } from 'src/shared/utilities/timestamp-to-date';


@Injectable()
export class EventsService {
    private db = admin.firestore();

    fetch(calendarName, monthKey): Promise<Array<EventModel>> {
        let eventsRef = this.db.collection(`calendars/${calendarName}/months/${monthKey}/events`)
        const events: Array<any> = [];
        return new Promise( (resolve) => {
            eventsRef.get().then(
                (snapshot) => {
                    resolve(snapshot.docs.map((doc): EventModel => {
                        
                        console.log(doc.id, '=>', doc.data())
                        return {
                            timeStart: timestampToDate(doc.data().timeStart._seconds),
                            timeEnd: timestampToDate(doc.data().timeEnd._seconds),
                            status: doc.data().status,
                            assignedUser: doc.data().assignedUser
                        }
                        
                    }))
                }
            ).catch((error) => {
                console.log(error)
            })
        });
    }

}
